﻿using Core.Dtos;
using FluentValidation;

namespace SuffrageApp.Validators
{
    public class OptionValidator : AbstractValidator<OptionDto>
    {
        const int TEXT_MIN_LENGTH = 1;
        const int TEXT_MAX_LENGTH = 30;

        public OptionValidator()
        {
            RuleFor(opt => opt.Id).NotNull();
            RuleFor(opt => opt.Text)
                .MinimumLength(TEXT_MIN_LENGTH)
                .MaximumLength(TEXT_MAX_LENGTH)
                .NotNull()
                .WithMessage("Option text must be filled.");
        }
    }
}
