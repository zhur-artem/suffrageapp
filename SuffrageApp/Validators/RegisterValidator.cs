﻿using FluentValidation;
using SuffrageApp.Models.Account;

namespace SuffrageApp.Validators
{
    public class RegisterValidator : AbstractValidator<RegisterViewModel>
    {
        public RegisterValidator()
        {
            RuleFor(registerForm => registerForm.Email).NotNull();
            RuleFor(registerForm => registerForm.Password).NotNull();
            RuleFor(registerForm => registerForm.PasswordConfirm).NotNull().Equal(registerForm => registerForm.Password);
        }
    }
}
