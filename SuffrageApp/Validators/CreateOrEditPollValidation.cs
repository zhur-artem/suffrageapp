﻿using FluentValidation;
using SuffrageApp.Models;

namespace SuffrageApp.Validators
{
    public class CreateOrEditPollValidation : AbstractValidator<CreatePollViewModel>
    {
        const int TITLE_LENGTH = 30;
        const int DESCRIPTION_LENGTH = 300;

        public CreateOrEditPollValidation()
        {
            RuleFor(poll => poll.PollDto.Id).NotNull();
            RuleFor(poll => poll.PollDto.StartDate)
                .NotNull()
                .WithMessage("Дата начала не может быть раньше текущего времени.");
            RuleFor(poll => poll.PollDto.EndDate)
                .GreaterThan(poll => poll.PollDto.StartDate)
                .NotNull()
                .WithMessage("Дата конца не может быть меньше даты начала.");
            RuleFor(poll => poll.PollDto.Description)
                .NotNull()
                .MaximumLength(DESCRIPTION_LENGTH)
                .WithMessage($"Количество символов в описании должно быть не больше {DESCRIPTION_LENGTH} символов.");
            RuleFor(poll => poll.PollDto.Title)
                .NotNull()
                .MaximumLength(TITLE_LENGTH)
                .WithMessage($"Количество символов в заголовке должно быть не больше {TITLE_LENGTH} символов.");
        }
    }
}
