﻿
using Core;
using Microsoft.AspNetCore.Authorization;
using SuffrageApp.Extensions;
using System.Threading.Tasks;

namespace SuffrageApp.Handlers
{
    public class ResourceHandler : AuthorizationHandler<CreatorRequirement, PollDto>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                       CreatorRequirement requirement,
                                                        PollDto poll)
        {
            if (context.User.GetUserId() == poll.Creator.Id)
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
