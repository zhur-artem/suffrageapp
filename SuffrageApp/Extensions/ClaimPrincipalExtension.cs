﻿using System.Security.Claims;

namespace SuffrageApp.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetUserName(this ClaimsPrincipal claims) => claims.FindFirstValue("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name");
        public static string GetUserId(this ClaimsPrincipal claims) => claims.FindFirstValue("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier");
    }
}
