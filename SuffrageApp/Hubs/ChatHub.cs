﻿using Core.Dtos;
using Core.Interfaces.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace SuffrageApp.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IMessageService _messageService;

        public ChatHub(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [Authorize]
        public async Task SendMessage(string message, string userId, string chatId)
        {
            MessageDto createdMessage = _messageService.CreateNewMessage(message, userId, Convert.ToInt32(chatId));
            await Clients.All.SendAsync("ReceiveMessage", createdMessage.Author.UserName, createdMessage.TextMessage, createdMessage.CreationDate.ToString("dd.MM HH:mm"));
        }
    }
}
