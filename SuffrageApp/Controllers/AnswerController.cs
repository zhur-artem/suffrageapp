﻿using Core;
using Core.Dtos;
using Core.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SuffrageApp.Extensions;

namespace SuffrageApp.Controllers
{
    public class AnswerController : Controller
    {

        private readonly IAnswerService _answerService;

        public AnswerController(IAnswerService answerService)
        {
            _answerService = answerService;
        }

        [HttpPost]
        [Authorize]
        public IActionResult Create(OptionDto Option, PollDto Poll)
        {
            var answer = new AnswerDto()
            {
                Option = Option,
                Author = new UserDto { Id = User.GetUserId() },
                Poll = Poll,
            };
            _answerService.Create(answer);

            return RedirectToAction("View", "Poll", new { id = Poll.Id });
        }
    }
}