﻿using Core;
using Core.Dtos;
using Core.Enums;
using Core.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SuffrageApp.Extensions;
using SuffrageApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Threading.Tasks;

namespace SuffrageApp.Controllers
{
    public class PollController : Controller
    {
        private const string CREATOR_POLICY = "CreatorPolicy";
        private readonly IPollService _pollService;
        private readonly IAuthorizationService _authorizationService;


        public PollController(IPollService pollService, IAuthorizationService authorizationService)
        {
            _pollService = pollService;
            _authorizationService = authorizationService;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Search(int page = 1,
                                    int pollsOnPage = 9,
                                    bool isDescending = true,
                                    StateEnum? status = null,
                                    string substringForSearch = null)
        {
            var filter = new SearchPollsModel()
            {
                IsDescending = isDescending,
                SearchSubstringInName = substringForSearch,
                Status = status,
            };

            var result = new PollsViewModel(_pollService.GetPollsPage(pollsOnPage, page, filter), User.GetUserId())
            {
                PageViewModel = new PageViewModel
                (
                    _pollService.GetPollsCount(filter),
                    page,
                    pollsOnPage
                ),
                isSearch = true,
                Filter = filter,
            };

            return View("Index", result);
        }

        [Authorize]
        public IActionResult Index(int page = 1, int pollsOnPage = 9)
        {
            var pollsViewModel = new PollsViewModel(_pollService.GetPollsPage(pollsOnPage, page), User.GetUserId())
            {
                PageViewModel = new PageViewModel
                (
                    _pollService.GetPollsCount(),
                    page,
                    pollsOnPage
                ),
            };

            return View(pollsViewModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult View(int id)
        {
            var pollView = new PollViewModel()
            {
                Poll = _pollService.GetPoll(id, User.GetUserId()),
                Users = _pollService.GetUsersExeptPollMembers(id)
            };
            return View(pollView);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Create()
        {
            var pollView = new CreatePollViewModel()
            {
                PollDto = new PollDto()
                {
                    StartDate = DateTime.Now.AddHours(2),
                    EndDate = DateTime.Now.AddDays(2)
                },
                IsEdit = false
            };

            return View(pollView);
        }

        [HttpPost]
        [Authorize]
        public IActionResult Create(CreatePollViewModel pollToCreate)
        {
            pollToCreate.PollDto.CreationDate = DateTime.Now;
            pollToCreate.PollDto.Creator = new UserDto()
            {
                UserName = User.GetUserName(),
                Id = User.GetUserId()
            };
            if (!ModelState.IsValid)
            {
                return View("Create", pollToCreate);
            }

            int createdPollId = _pollService.CreatePollAndGetId(pollToCreate.PollDto);

            return RedirectToAction("View", new { id = createdPollId });
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Edit(int id)
        {
            var pollView = new CreatePollViewModel()
            {
                PollDto = _pollService.GetPoll(id, User.GetUserId()),
                IsEdit = true
            };

            var authorizationResult = await _authorizationService.AuthorizeAsync(User, pollView.PollDto, CREATOR_POLICY);

            if (IsUserOwner(authorizationResult.Succeeded) && pollView.PollDto.State != Core.Enums.StateEnum.Active)
            {
                return View("Create", pollView);
            }
            return new BadRequestResult();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Edit(CreatePollViewModel pollViewToUpdate)
        {
            if (!ModelState.IsValid)
            {
                return View("Create", pollViewToUpdate);
            }

            var authorizationResult = await _authorizationService.AuthorizeAsync(User, pollViewToUpdate.PollDto, CREATOR_POLICY);

            if (IsUserOwner(authorizationResult.Succeeded))
            {
                pollViewToUpdate.PollDto.Options.ForEach(opt => opt.Poll = pollViewToUpdate.PollDto);
                _pollService.UpdatePoll(pollViewToUpdate.PollDto);

                return RedirectToAction("View", "Poll", new { id = pollViewToUpdate.PollDto.Id });
            }
            return new BadRequestResult();
        }

        [HttpGet]
        [Authorize]
        public IActionResult Delete(int id)
        {
            _pollService.DeletePoll(id);

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> StartNow(int id)
        {
            var poll = _pollService.GetPoll(id, User.GetUserId());
            var authorizationResult = await _authorizationService.AuthorizeAsync(User, poll, CREATOR_POLICY);

            if (IsUserOwner(authorizationResult.Succeeded))
            {
                _pollService.StartPollNow(id);
                return RedirectToAction("View", "Poll", new { id = id });
            }
            return new BadRequestResult();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> InviteUsersToPoll(int pollId, IEnumerable<string> userIds)
        {
            var poll = _pollService.GetPoll(pollId, User.GetUserId());
            var authorizationResult = await _authorizationService.AuthorizeAsync(User, poll, CREATOR_POLICY);
            if (IsUserOwner(authorizationResult.Succeeded))
            {
                _pollService.InviteUsersToPoll(pollId, userIds.Except(poll.Members.Select(m => m.Id)).ToList());
                return RedirectToAction("View", "Poll", new { id = pollId });
            }
            return new BadRequestResult();
        }

        private bool IsUserOwner(bool isSucceeded)
        {
            if (isSucceeded)
            {
                return true;
            }
            else if (User.Identity.IsAuthenticated)
            {
                throw new SecurityException("You are not the owner of this poll.");
            }
            else
            {
                throw new UnauthorizedAccessException("You are not logged in.");
            }
        }
    }
}
