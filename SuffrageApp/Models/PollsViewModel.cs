﻿using Core;
using Core.Dtos;
using System.Collections.Generic;
using System.Linq;

namespace SuffrageApp.Models
{
    public class PollsViewModel
    {
        public List<PollDto> Polls { get; }
        public PageViewModel PageViewModel { get; set; }
        public IDictionary<PollDto, bool> IsUserHaveAccess { get; set; }
        public bool isSearch { get; set; } = false;
        public SearchPollsModel Filter { get; set; }

        public PollsViewModel(List<PollDto> polls, string userId)
        {
            Polls = polls;
            IsUserHaveAccess = new Dictionary<PollDto, bool>();

            foreach (var poll in Polls)
            {
                if (poll.IsPrivate && poll.Members.FirstOrDefault(m => m.Id == userId) != null)
                {
                    IsUserHaveAccess.Add(poll, true);
                }
                else
                {
                    IsUserHaveAccess.Add(poll, false);
                }
            }
        }
    }
}
