﻿using System.ComponentModel.DataAnnotations;

namespace SuffrageApp.Models.Account
{
    public class RegisterViewModel
    {
        public string Email { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }


        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}
