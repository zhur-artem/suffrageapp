﻿using Core;
using Core.Dtos;
using System.Collections.Generic;

namespace SuffrageApp.Models
{
    public class PollViewModel
    {
        public PollDto Poll { get; set; }
        public List<UserDto> Users { get; set; }
    }
}
