﻿using Core;

namespace SuffrageApp.Models
{
    public class CreatePollViewModel
    {
        public bool IsEdit { get; set; }

        public PollDto PollDto { get; set; }
    }
}
