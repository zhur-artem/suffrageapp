﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;

connection.on("ReceiveMessage", function (user, message, date) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var username = document.createElement("span");
    username.classList.add("text-primary");
    username.append(user);
    var dateSpan = document.createElement("span");
    dateSpan.classList.add("text-muted");
    dateSpan.append(date);
    var li = document.createElement("li");

    li.classList.add("list-group-item");
    li.classList.add("animated");
    li.classList.add("fadeInLeft");
    li.append(dateSpan, " - ", username, ": ", msg, "\n");
    var messageList = document.getElementById("messagesList");
    messageList.appendChild(li);
    messageList.scrollTop = messageList.scrollHeight;
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var userId = document.getElementById("userInput").value;
    var chatId = document.getElementById("chatInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", message, userId, chatId).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});