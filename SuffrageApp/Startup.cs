using AutoMapper;
using Core.Entities;
using Core.Interfaces.IRepositories;
using Core.Interfaces.IServices;
using Core.Interfaces.Services;
using Core.Mapping;
using Core.Services;
using FluentValidation.AspNetCore;
using Infrastructure.Data;
using Infrastructure.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SuffrageApp.Handlers;
using SuffrageApp.Hubs;
using SuffrageApp.Validators;

namespace SuffrageApp
{
    public class Startup
    {
        public Startup(IConfiguration config) => this.Configuration = config;

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddMvc()
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssemblyContaining<CreateOrEditPollValidation>();
                    fv.RegisterValidatorsFromAssemblyContaining<RegisterValidator>();
                })
                .AddRazorRuntimeCompilation()
                .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0);

            services.AddAutoMapper(typeof(PollProfile));

            services.AddDbContext<AppDbContext>(o =>
                    {
                        o.UseSqlServer(Configuration.GetConnectionString("SuffrageAppDb"));
                    });

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>();
            services.AddAuthorization(options =>
            {
                options.AddPolicy("CreatorPolicy", policy =>
                    policy.Requirements.Add(new CreatorRequirement()));
            });

            services
                .AddTransient<IPollRepository, PollRepository>()
                .AddTransient<IPollService, PollService>()
                .AddTransient<IAnswerService, AnswerService>()
                .AddTransient<IAnswerRepository, AnswerRepository>()
                .AddTransient<IOptionRepository, OptionRepository>()
                .AddTransient<IMemberRepository, MemberRepository>()
                .AddTransient<IMessageRepository, MessageRepository>()
                .AddTransient<IMessageService, MessageService>()
                .AddTransient<IChatRepository, ChatRepository>()
                .AddSingleton<IAuthorizationHandler, ResourceHandler>();

            services.AddSignalR(options =>
            {
                options.EnableDetailedErrors = true;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapHub<ChatHub>("/chatHub");
            });
        }
    }
}
