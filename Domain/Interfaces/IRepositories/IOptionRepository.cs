﻿using Core.Entities;

namespace Core.Interfaces.IRepositories
{
    public interface IOptionRepository : IRepository<Option>
    {
        public Option GetOptionByText(string optionText);
        public Option GetOptionWithPollAndMembers(int id);
    }
}
