﻿using Core.Dtos;
using Core.Entities;
using System.Collections.Generic;

namespace Core.Interfaces.IRepositories
{
    public interface IPollRepository : IRepository<Poll>
    {
        public List<Poll> GetPollsCount(SearchPollsModel filter = null);
        public List<Poll> GetPollsPage(int pollOnPage, int page);
        public List<Poll> GetPollsPage(int pollOnPage, int page, SearchPollsModel filter);
        Poll GetPollWithOptions(int id);
        public void UpdatePollWithOptionsAndChat(Poll pollForUpdate);

        void StartPollNow(int id);

    }
}
