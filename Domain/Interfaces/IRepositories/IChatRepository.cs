﻿using Core.Entities.PollChat;

namespace Core.Interfaces.IRepositories
{
    public interface IChatRepository : IRepository<Chat>
    {
    }
}
