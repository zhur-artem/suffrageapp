﻿using Core.Entities;

namespace Core.Interfaces.IRepositories
{
    public interface IAnswerRepository : IRepository<Answer>
    {

    }
}
