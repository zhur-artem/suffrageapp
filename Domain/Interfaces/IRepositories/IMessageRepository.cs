﻿using Core.Entities.PollChat;

namespace Core.Interfaces.IRepositories
{
    public interface IMessageRepository : IRepository<Message>
    {
    }
}
