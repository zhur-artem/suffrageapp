﻿using Core.Entities;
using Core.Entities.ManyToMany;
using System.Collections.Generic;

namespace Core.Interfaces.IRepositories
{
    public interface IMemberRepository : IRepository<Member>
    {
        public bool UpdateIfUserIsPollMember(Member userPolls);
        public bool IsUserMemberInPoll(Member userPolls);

        public List<Member> GetPollMembers(int pollId);
        public void AddUserInPollAsMember(int pollId, List<string> userId);

        public List<User> GetUsersExeptPollMembers(int pollId);
    }
}
