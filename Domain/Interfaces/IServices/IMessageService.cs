﻿using Core.Dtos;
using Core.Entities.PollChat;
using System.Collections.Generic;

namespace Core.Interfaces.IServices
{
    public interface IMessageService
    {
        public MessageDto CreateNewMessage(string textMessage, string userId, int chatId);
        public bool DeleteMessage(int messageId);
        public List<Message> GetChatMessages(int ChatId);
    }
}
