﻿using Core.Dtos;
using System.Collections.Generic;

namespace Core.Interfaces.Services
{
    public interface IAnswerService
    {
        bool Create(AnswerDto answer);

        List<AnswerDto> GetAnswersByUser(int id);
        List<AnswerDto> GetAnswersByPoll(int id);
    }
}
