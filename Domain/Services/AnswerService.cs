﻿using AutoMapper;
using Core.Dtos;
using Core.Entities;
using Core.Entities.ManyToMany;
using Core.Interfaces.IRepositories;
using Core.Interfaces.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Services
{
    public class AnswerService : IAnswerService
    {
        private readonly IMapper _mapper;
        private readonly IMemberRepository _memberRepository;
        private readonly IAnswerRepository _answerRepository;
        private readonly IOptionRepository _optionRepository;
        private readonly UserManager<User> _userManager;
        private readonly IPollRepository _pollRepository;

        public AnswerService(IMapper mapper,
            IAnswerRepository answerRepository,
            IOptionRepository optionRepository,
            IMemberRepository userPollsRepository,
            IPollRepository pollRepository,
            UserManager<User> userManager)
        {
            _mapper = mapper;
            _answerRepository = answerRepository;
            _optionRepository = optionRepository;
            _memberRepository = userPollsRepository;
            _userManager = userManager;
            _pollRepository = pollRepository;
        }

        public bool Create(AnswerDto answerDto)
        {
            var answer = _mapper.Map<Answer>(answerDto);
            answer.Author = _userManager.FindByIdAsync(answer.Author.Id).Result;

            answer.Poll = _pollRepository.GetPollWithOptions(answerDto.Poll.Id);
            answer.Option = answer.Poll.Options.FirstOrDefault(op => op.Id == answer.Option.Id);

            if (IsAnswerValid(answer))
            {
                var member = new Member
                {
                    Poll = answer.Option.Poll,
                    User = answer.Author,
                    isVoted = true
                };

                bool updated = _memberRepository.UpdateIfUserIsPollMember(member);

                if (!updated)
                {
                    _memberRepository.Add(member);
                }
                return _answerRepository.Add(answer) > 0;
            }
            return false;
        }

        public List<AnswerDto> GetAnswersByPoll(int id)
        {
            throw new NotImplementedException();
        }

        public List<AnswerDto> GetAnswersByUser(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Пользователь может проголосовать когда
        /// 1. Он не голосовал ранее в этом опросе
        /// 2. Пункт принадлежит опросу
        /// 3. Если опрос имеет статус Started
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        private bool IsAnswerValid(Answer answer)
        {
            User user = _userManager.FindByIdAsync(answer.Author.Id).Result;
            Option option = _optionRepository.GetOptionWithPollAndMembers(answer.Option.Id);
            //TODO: валидация ответа
            //Пользователь является участником опроса
            //if (!option.Poll.Members.Where(member => member.User.Id == user.Id).Any())
            //{
            //    return false;
            //}

            //
            //if (!option.Poll)
            //{
            //    return false;
            //}

            return true;
        }
    }
}
