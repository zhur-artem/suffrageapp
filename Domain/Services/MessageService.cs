﻿using AutoMapper;
using Core.Dtos;
using Core.Entities;
using Core.Entities.PollChat;
using Core.Interfaces.IRepositories;
using Core.Interfaces.IServices;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Core.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IPollRepository _pollRepository;
        private readonly IChatRepository _chatRepository;
        private readonly IMessageRepository _messageRepository;


        public MessageService(IMapper mapper,
            IPollRepository pollRepository,
            IChatRepository chatRepository,
            IMessageRepository messageRepository,
            UserManager<User> userManager)
        {
            _mapper = mapper;
            _userManager = userManager;
            _pollRepository = pollRepository;
            _chatRepository = chatRepository;
            _messageRepository = messageRepository;
        }

        public MessageDto CreateNewMessage(string textMessage, string username, int chatId)
        {
            var user = _userManager.FindByNameAsync(username).Result;
            var message = new Message()
            {
                TextMessage = textMessage,
                Author = user,
                Chat = GetExistingChat(chatId),
                CreationDate = DateTime.Now,
            };
            _messageRepository.Add(message);

            return _mapper.Map<MessageDto>(message);
        }

        private Chat GetExistingChat(int id)
        {
            return _chatRepository.GetById(id);
        }

        public bool DeleteMessage(int messageId)
        {
            throw new NotImplementedException();
        }

        public List<Message> GetChatMessages(int ChatId)
        {
            throw new NotImplementedException();
        }
    }
}
