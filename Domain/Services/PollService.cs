﻿using AutoMapper;
using Core.Dtos;
using Core.Entities;
using Core.Entities.ManyToMany;
using Core.Entities.PollChat;
using Core.Extentions;
using Core.Interfaces.IRepositories;
using Core.Interfaces.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;

namespace Core.Services
{
    public class PollService : IPollService
    {
        private readonly IMapper _mapper;
        private readonly IPollRepository _pollRepository;
        private readonly IOptionRepository _optionRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMemberRepository _memberRepository;

        public PollService(IMapper mapper,
            IPollRepository pollRepository,
            IOptionRepository optionRepository,
            UserManager<User> userManager,
            IMemberRepository memberRepository)
        {
            _mapper = mapper;
            _pollRepository = pollRepository;
            _optionRepository = optionRepository;
            _userManager = userManager;
            _memberRepository = memberRepository;
        }

        public List<PollDto> GetPollsPage(int pollsOnPage, int page, SearchPollsModel filter = null)
        {
            var result = new List<PollDto>();
            if (filter != null)
            {
                result = _mapper.Map<List<PollDto>>(_pollRepository.GetPollsPage(pollsOnPage, page, filter));
            }
            else
            {
                result = _mapper.Map<List<PollDto>>(_pollRepository.GetPollsPage(pollsOnPage, page));
            }

            result.ForEach(poll => poll.State = poll.GetPollState());

            return result ?? new List<PollDto>();
        }

        /// <summary>
        /// Получить опрос по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор опроса</param>
        /// <returns></returns>
        public PollDto GetPoll(int id, string currentUserId)
        {
            var poll = _pollRepository.GetPollWithOptions(id);
            if (poll == null)
            {
                throw new NullReferenceException("There is no such poll.");
            }
            poll.Members = _memberRepository.GetPollMembers(id);
            PollDto pollDto = _mapper.Map<PollDto>(poll);
            pollDto.State = pollDto.GetPollState();

            if (poll.IsPrivate)
            {
                if (poll.Members.FirstOrDefault(memb => memb.User.Id == currentUserId) != null)
                {
                    return pollDto;
                }
                else
                {
                    throw new SecurityException("You are not invited to this poll.");
                }
            }
            else
            {
                return pollDto ?? new PollDto();
            }
        }

        public List<User> GetUsersFromPoll(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Создание опроса 
        /// </summary>
        /// <param name="dto">Временная сущность создаваемого опроса</param>
        /// <returns>Идентификатор созданного опроса</returns>
        public int CreatePollAndGetId(PollDto dto)
        {
            var mappedPoll = _mapper.Map<Poll>(dto);
            mappedPoll.Creator = _userManager.FindByIdAsync(dto.Creator.Id).Result;
            if (mappedPoll.IsChatEnabled)
            {
                mappedPoll.Chat = new Chat()
                {
                    Poll = mappedPoll,
                    Users = new List<User>() {
                        mappedPoll.Creator
                    },
                };
            }

            var createdPollId = _pollRepository.Add(mappedPoll);

            _memberRepository.Add(new Member()
            {
                Poll = mappedPoll,
                User = mappedPoll.Creator,
                isVoted = false
            });

            return createdPollId;
        }

        public bool UpdatePoll(PollDto dto)
        {
            dto.State = dto.GetPollState();
            var mappedPoll = _mapper.Map<Poll>(dto);
            _pollRepository.UpdatePollWithOptionsAndChat(mappedPoll);

            return true;
        }

        public bool DeletePoll(int id)
        {
            try
            {
                _pollRepository.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public int GetPollsCount(SearchPollsModel filter = null)
        {
            var result = _mapper.Map<List<PollDto>>(_pollRepository.GetPollsCount(filter));
            if (filter?.Status != null)
            {
                result.ForEach(poll => poll.State = poll.GetPollState());
                result = result.Where(poll => poll.State == filter.Status).ToList();
            }

            return result.Count();
        }

        public bool StartPollNow(int id)
        {
            _pollRepository.StartPollNow(id);

            return true;
        }

        public void InviteUsersToPoll(int pollId, List<string> userIds)
        {
            _memberRepository.AddUserInPollAsMember(pollId, userIds);
        }

        public List<UserDto> GetUsersExeptPollMembers(int pollId) //temporary solution
        {
            return _mapper.Map<List<UserDto>>(_memberRepository.GetUsersExeptPollMembers(pollId));
        }
    }
}
