﻿using System.Collections.Generic;

namespace Core.SharedKernel
{
    public class BaseEntity
    {
        public int Id { get; set; }

        public List<BaseEvent> Events = new List<BaseEvent>();
    }
}
