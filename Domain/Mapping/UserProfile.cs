﻿using AutoMapper;
using Core.Dtos;
using Core.Entities;
using Core.Entities.ManyToMany;

namespace Core.Mapping
{
    class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, User>()
                .ForMember(ud => ud.Polls, u => u.MapFrom(u => u.Polls))
                .ReverseMap();

            CreateMap<Member, UserDto>()
                .ForMember(u => u.UserName, m => m.MapFrom(m => m.User.UserName))
                .ForMember(u => u.Polls, m => m.MapFrom(m => m.User.Polls))
                .ForMember(u => u.Answers, m => m.MapFrom(m => m.User.Answers))
                .ForMember(u => u.Id, m => m.MapFrom(m => m.User.Id))
                .ReverseMap();
        }
    }
}
