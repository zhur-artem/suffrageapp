﻿using AutoMapper;
using Core.Dtos;
using Core.Entities.PollChat;

namespace Core.Mapping
{
    class ChatProfile : Profile
    {
        public ChatProfile()
        {
            CreateMap<MessageDto, Message>().ReverseMap();
            CreateMap<Chat, ChatDto>().ReverseMap();
        }
    }
}
