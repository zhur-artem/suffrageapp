﻿using AutoMapper;
using Core.Dtos;
using Core.Entities;

namespace Core.Mapping
{
    public class OptionProfile : Profile
    {
        public OptionProfile()
        {
            CreateMap<OptionDto, Option>();
            CreateMap<Option, OptionDto>();
        }
    }
}
