﻿using AutoMapper;
using Core.Dtos;
using Core.Entities;

namespace Core.Mapping
{
    public class AnswerProfile : Profile
    {
        public AnswerProfile()
        {
            CreateMap<AnswerDto, Answer>();
            CreateMap<Answer, AnswerDto>();
        }
    }
}
