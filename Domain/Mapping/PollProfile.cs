﻿using AutoMapper;
using Core.Entities;
using Core.Entities.ManyToMany;

namespace Core.Mapping
{
    public class PollProfile : Profile
    {
        public PollProfile()
        {
            CreateMap<Poll, PollDto>().ReverseMap();
            CreateMap<PollDto, Member>().ForMember(m => m.Poll, pd => pd.Ignore()).ReverseMap();
        }
    }
}
