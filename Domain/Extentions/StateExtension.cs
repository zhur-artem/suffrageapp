﻿using Core.Enums;
using System;

namespace Core.Extentions
{
    public static class StateExtension
    {
        public static StateEnum GetPollState(this PollDto poll)
        {
            //Если дата начала в прошлом - опрос начался, иначе дата начала в будущем и опрос ещё не начался
            StateEnum result = poll.StartDate <= DateTime.Now ? StateEnum.Active : StateEnum.NotStarted;

            if (poll.EndDate <= DateTime.Now) //Если дата конца в прошлом - опрос закончился
            {
                result = StateEnum.Ended;
            }
            return result;
        }
    }
}
