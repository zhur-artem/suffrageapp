﻿using Core.SharedKernel;

namespace Core.Dtos
{
    public class AnswerDto : BaseDto
    {
        /// <summary>
        /// Автор голоса
        /// </summary>
        public UserDto Author { get; set; }

        /// <summary>
        /// Автор выбрал вот этот ответ
        /// </summary>
        public OptionDto Option { get; set; }
        public PollDto Poll { get; set; }

    }
}
