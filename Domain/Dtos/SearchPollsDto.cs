﻿using Core.Enums;

namespace Core.Dtos
{
    public class SearchPollsModel
    {
        /// <summary>
        /// Сортировка по убыванию?
        /// </summary>
        public bool IsDescending { get; set; }

        /// <summary>
        /// Статус искомых опросов
        /// </summary>
        public StateEnum? Status { get; set; }

        /// <summary>
        /// Строка, искомая в существующих опросах
        /// </summary>
        public string SearchSubstringInName { get; set; }
    }
}
