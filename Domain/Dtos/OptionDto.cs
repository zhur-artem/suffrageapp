﻿using Core.SharedKernel;

namespace Core.Dtos
{
    public class OptionDto : BaseDto
    {

        /// <summary>
        /// Текст варианта ответа
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Вариант ответа принадлежит этому опросу
        /// </summary>
        public PollDto Poll { get; set; }
    }
}
