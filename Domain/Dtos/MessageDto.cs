﻿
using Core.SharedKernel;
using System;

namespace Core.Dtos
{
    public class MessageDto : BaseDto
    {

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string TextMessage { get; set; }

        /// <summary>
        /// Автор сообщения
        /// </summary>
        public UserDto Author { get; set; }

        /// <summary>
        /// Чат, в котором находится данное сообщение
        /// </summary>
        public ChatDto Chat { get; set; }

        public DateTime CreationDate { get; set; }
    }
}
