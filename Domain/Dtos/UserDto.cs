﻿using System.Collections.Generic;

namespace Core.Dtos
{
    public class UserDto
    {
        public string Id { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Опросы, которые созданы пользователем
        /// </summary>
        public List<PollDto> Polls { get; set; }

        /// <summary>
        /// Все голоса пользователя, которые он выбрал в разных опросах
        /// </summary>
        public List<AnswerDto> Answers { get; set; }
        public bool IsVoted { get; set; }
    }
}
