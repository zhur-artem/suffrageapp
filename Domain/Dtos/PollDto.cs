﻿using Core.Dtos;
using Core.Enums;
using Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core
{
    public class PollDto : BaseDto
    {
        /// <summary>
        /// Заголовок
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [DataType(DataType.DateTime)]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Дата начала опроса
        /// </summary>
        [DataType(DataType.DateTime)]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата конца опроса
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Голоса пользователей в опросе
        /// </summary>
        public List<AnswerDto> Answers { get; set; }

        /// <summary>
        /// Возможные варианты ответов пользователем
        /// </summary>
        public List<OptionDto> Options { get; set; }

        /// <summary>
        /// Состояние лота
        /// </summary>
        public StateEnum State { get; set; }

        /// <summary>
        /// Создатель опроса
        /// </summary>
        public UserDto Creator { get; set; }

        /// <summary>
        /// Участники опроса
        /// </summary>
        public List<UserDto> Members { get; set; }

        public ChatDto Chat { get; set; }

        public bool IsPrivate { get; set; }

        public bool IsChatEnabled { get; set; }
    }
}
