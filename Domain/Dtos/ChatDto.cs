﻿using Core.SharedKernel;
using System.Collections.Generic;

namespace Core.Dtos
{
    public class ChatDto : BaseDto
    {
        public List<MessageDto> Messages { get; set; }

        public List<UserDto> Users { get; set; }

        public PollDto Poll { get; set; }
    }
}
