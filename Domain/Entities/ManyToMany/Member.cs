﻿using Core.SharedKernel;

namespace Core.Entities.ManyToMany
{
    public class Member : BaseEntity
    {
        public bool isVoted { get; set; }

        public User User { get; set; }

        public Poll Poll { get; set; }
    }
}
