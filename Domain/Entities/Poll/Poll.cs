﻿using Core.Entities.ManyToMany;
using Core.Entities.PollChat;
using Core.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    /// <summary>
    /// Опрос
    /// </summary>
    public class Poll : BaseEntity
    {
        /// <summary>
        /// Заголовок
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [Required]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Дата начала опроса
        /// </summary>
        [Required]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата конца опроса
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Голоса пользователей в опросе
        /// </summary>
        public List<Answer> Answers { get; set; }

        /// <summary>
        /// Возможные варианты ответов пользователем
        /// </summary>
        public List<Option> Options { get; set; }

        /// <summary>
        /// Создатель опроса
        /// </summary>
        [Required]
        public User Creator { get; set; }

        /// <summary>
        /// Участники опроса
        /// </summary>
        public List<Member> Members { get; set; }

        public Chat Chat { get; set; }

        public bool IsPrivate { get; set; }

        public bool IsChatEnabled { get; set; }
    }
}
