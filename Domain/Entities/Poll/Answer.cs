﻿using Core.SharedKernel;

namespace Core.Entities
{
    /// <summary>
    /// Выбранный пользователем вариант для какого-то опроса
    /// </summary>
    public class Answer : BaseEntity
    {
        /// <summary>
        /// Автор голоса
        /// </summary>
        public User Author { get; set; }

        /// <summary>
        /// Голос принадлежит данному вопросу
        /// </summary>
        public Option Option { get; set; }

        public Poll Poll { get; set; }

    }
}
