﻿using Core.SharedKernel;

namespace Core.Entities
{
    public class Option : BaseEntity
    {
        /// <summary>
        /// Текст варианта ответа
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Вариант ответа принадлежит этому опросу
        /// </summary>
        public Poll Poll { get; set; }
    }
}
