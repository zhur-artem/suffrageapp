﻿
using Core.Entities.ManyToMany;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Core.Entities
{
    public class User : IdentityUser
    {
        /// <summary>
        /// Ответы пользователя
        /// </summary>
        public List<Answer> Answers { get; set; }

        /// <summary>
        /// Опросы пользователя
        /// </summary>
        public List<Member> Polls { get; set; }
    }
}
