﻿using Core.SharedKernel;
using System.Collections.Generic;

namespace Core.Entities.PollChat
{
    public class Chat : BaseEntity
    {
        public List<Message> Messages { get; set; }

        public List<User> Users { get; set; }

        public int PollId { get; set; }
        public Poll Poll { get; set; }
    }
}
