﻿using Core.Entities;
using Core.Entities.ManyToMany;
using Core.Entities.PollChat;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Poll> Polls { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Chat> Chats { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Poll>()
              .HasMany(poll => poll.Options)
              .WithOne(option => option.Poll)
              .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Member>()
                .HasOne(up => up.User)
                .WithMany(u => u.Polls);

            modelBuilder.Entity<Member>()
                .HasOne(up => up.Poll)
                .WithMany(poll => poll.Members);

            modelBuilder.Entity<Poll>()
                .HasOne(p => p.Chat)
                .WithOne(c => c.Poll)
                .HasForeignKey<Chat>(p => p.PollId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Chat>()
                .HasMany(p => p.Messages)
                .WithOne(p => p.Chat)
                .OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            int result = base.SaveChanges();

            return result;
        }
    }
}
