﻿using Core.Entities.PollChat;
using Core.Interfaces.IRepositories;
using Infrastructure.Data.Repositories.Base;

namespace Infrastructure.Data.Repositories
{
    public class MessageRepository : EfRepository<Message>, IMessageRepository
    {
        private readonly AppDbContext _dbContext;

        public MessageRepository(AppDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }


    }
}
