﻿using Core.Entities;
using Core.Interfaces.IRepositories;
using Infrastructure.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Infrastructure.Data.Repositories
{
    public class OptionRepository : EfRepository<Option>, IOptionRepository
    {
        private readonly AppDbContext _dbContext;

        public OptionRepository(AppDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public Option GetOptionByText(string optionText)
        {
            var result = _dbContext.Options
                .Where(option => option.Text.Equals(optionText))
                .ToList();

            if (result.Count > 1)
            {
                throw new Exception("Вариантов ответа с таким текстом несколько.");
            }

            return result.FirstOrDefault();
        }

        public Option GetOptionWithPollAndMembers(int id)
        {
            var result = _dbContext.Options
                .Include(opt => opt.Poll)
                .ThenInclude(poll => poll.Members)
                .FirstOrDefault(option => option.Id == id);

            if (result.Poll != null)
            {
                return result;
            }

            throw new Exception("The voting option has no parent poll.");
        }
    }
}
