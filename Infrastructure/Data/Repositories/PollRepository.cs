﻿using Core.Dtos;
using Core.Entities;
using Core.Entities.PollChat;
using Core.Enums;
using Core.Interfaces.IRepositories;
using Infrastructure.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Data.Repositories
{
    public class PollRepository : EfRepository<Poll>, IPollRepository
    {
        private readonly AppDbContext _dbContext;

        public PollRepository(AppDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Poll> GetPollsCount(SearchPollsModel filter = null)
        {
            if (filter?.SearchSubstringInName != null)
            {
                return _dbContext.Polls
                    .Where(poll =>
                       poll.Title.Contains(filter.SearchSubstringInName) ||
                       poll.Description.Contains(filter.SearchSubstringInName))
                    .ToList();
            }
            else
            {
                return _dbContext.Polls.ToList();
            }
        }

        public List<Poll> GetPollsPage(int pollOnPage, int page)
        {
            return _dbContext.Polls
                .OrderByDescending(poll => poll.Id)
                .Skip((page - 1) * pollOnPage)
                .Take(pollOnPage)
                .Include(p => p.Members)
                .ThenInclude(m => m.User)
                .ToList();
        }

        public List<Poll> GetPollsPage(int pollOnPage, int page, SearchPollsModel filter)
        {
            IIncludableQueryable<Poll, User> result;
            IQueryable<Poll> search = _dbContext.Polls;

            if (filter.SearchSubstringInName != null)
            {
                search = search.Where(poll =>
                     poll.Title.Contains(filter.SearchSubstringInName) ||
                     poll.Description.Contains(filter.SearchSubstringInName));
            }

            if (filter.IsDescending)
            {
                search.OrderByDescending(poll => poll.Id);
            }

            if (filter.Status != null)
            {
                switch (filter.Status)
                {
                    case StateEnum.Active:
                        search = search.Where(p => p.StartDate < DateTime.Now && DateTime.Now < p.EndDate);
                        break;
                    case StateEnum.NotStarted:
                        search = search.Where(p => p.StartDate > DateTime.Now);
                        break;
                    case StateEnum.Ended:
                        search = search.Where(p => p.EndDate <= DateTime.Now);
                        break;
                    default:
                        break;
                }
            }

            result = search.Skip((page - 1) * pollOnPage)
                           .Take(pollOnPage)
                           .Include(p => p.Members)
                           .ThenInclude(m => m.User);

            return result.ToList();
        }

        public Poll GetPollWithOptions(int id)
        {
            return _dbContext.Polls
                .Include(poll => poll.Creator)
                .Include(poll => poll.Options)
                .Include(poll => poll.Answers)
                .Include(poll => poll.Chat)
                .ThenInclude(chat => chat.Messages)
                .ThenInclude(message => message.Author)
                .SingleOrDefault(poll => poll.Id == id);
        }

        public void StartPollNow(int id)
        {
            Poll existingPoll = _dbContext.Polls.Where(p => p.Id == id).FirstOrDefault();
            if (existingPoll != null)
            {
                existingPoll.StartDate = DateTime.Now;
            }
            _dbContext.SaveChanges();
        }

        public void UpdatePollWithOptionsAndChat(Poll pollForUpdate)
        {
            var existingPoll = _dbContext.Polls
                .Where(p => p.Id == pollForUpdate.Id)
                .Include(p => p.Options)
                .Include(p => p.Chat)
                .Include(p => p.Creator)
                .SingleOrDefault();

            if (existingPoll != null)
            {
                _dbContext.Entry(existingPoll).CurrentValues.SetValues(pollForUpdate);

                foreach (var existingChild in existingPoll.Options.ToList())
                {
                    if (!pollForUpdate.Options.Any(c => c.Id == existingChild.Id))
                        _dbContext.Options.Remove(existingChild);
                }

                foreach (var option in pollForUpdate.Options)
                {
                    if (option.Id != 0)
                    {
                        _dbContext.Entry(option).CurrentValues.SetValues(option);
                    }
                    else
                    {
                        var newChild = new Option
                        {
                            Text = option.Text,
                            Poll = option.Poll
                        };
                        existingPoll.Options.Add(newChild);
                    }
                }

                if (pollForUpdate.IsChatEnabled && existingPoll.Chat == null)
                {
                    existingPoll.Chat = new Chat
                    {
                        Poll = existingPoll,
                        PollId = existingPoll.Id,
                        Users = new List<User>() {
                            existingPoll.Creator
                        },
                    };
                }
                else if (!pollForUpdate.IsChatEnabled && existingPoll.Chat != null)
                {
                    _dbContext.Chats.Remove(existingPoll.Chat);
                }

                _dbContext.SaveChanges();
            }
        }
    }
}
