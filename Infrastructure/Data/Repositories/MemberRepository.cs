﻿using Core.Entities;
using Core.Entities.ManyToMany;
using Core.Interfaces.IRepositories;
using Infrastructure.Data.Repositories.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Data.Repositories
{
    public class MemberRepository : EfRepository<Member>, IMemberRepository
    {
        private readonly AppDbContext _dbContext;

        public MemberRepository(AppDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public bool UpdateIfUserIsPollMember(Member userPolls)
        {
            try
            {
                var existingEntity = _dbContext.Members
                    .Where(m => m.Poll == userPolls.Poll &&
                                m.User == userPolls.User)
                    .FirstOrDefault();

                existingEntity.isVoted = userPolls.isVoted;
                _dbContext.Update(existingEntity);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsUserMemberInPoll(Member userPolls)
        {
            var existingEntity = _dbContext.Members
                .Where(m => m.Poll == userPolls.Poll &&
                            m.User == userPolls.User);

            return existingEntity.Any();
        }

        public List<Member> GetPollMembers(int pollId)
        {
            var result = _dbContext.Members
                .Include(m => m.User)
                .Where(m => m.Poll.Id == pollId).ToList();

            return result;
        }

        public void AddUserInPollAsMember(int pollId, List<string> userIds)
        {
            foreach (var user in userIds)
            {
                var newMember = new Member()
                {
                    isVoted = false,
                    Poll = _dbContext.Polls.Find(pollId),
                    User = _dbContext.Users.Find(user)
                };
                _dbContext.Add(newMember);
            }
            _dbContext.SaveChanges();
        }

        public List<User> GetUsersExeptPollMembers(int pollId) //temporary solution
        {
            var pollMembers = (IEnumerable<Member>)GetPollMembers(pollId);

            return _dbContext.Users.ToList().Except(pollMembers.Select(m => m.User)).ToList();
        }
    }
}
