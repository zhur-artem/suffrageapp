﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class settingsNameChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isChatEnable",
                table: "Polls");

            migrationBuilder.RenameColumn(
                name: "isPrivate",
                table: "Polls",
                newName: "IsPrivate");

            migrationBuilder.AddColumn<bool>(
                name: "IsChatEnabled",
                table: "Polls",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsChatEnabled",
                table: "Polls");

            migrationBuilder.RenameColumn(
                name: "IsPrivate",
                table: "Polls",
                newName: "isPrivate");

            migrationBuilder.AddColumn<bool>(
                name: "isChatEnable",
                table: "Polls",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
