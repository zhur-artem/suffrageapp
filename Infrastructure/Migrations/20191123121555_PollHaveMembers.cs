﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class PollHaveMembers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PollId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_PollId",
                table: "AspNetUsers",
                column: "PollId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Polls_PollId",
                table: "AspNetUsers",
                column: "PollId",
                principalTable: "Polls",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Polls_PollId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_PollId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PollId",
                table: "AspNetUsers");
        }
    }
}
